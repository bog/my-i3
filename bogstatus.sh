#!/usr/bin/env bash

# By BOG

COLOR_SUCCESS="#00ff00"
COLOR_FAILURE="#ff0000"
COLOR_WARNING="#ffff00"
COLOR_INFO="#0000ff"

function begin()
{
    echo -n ',['
    echo -n '{"name":"","full_text":""}'
}

function end()
{
    echo -n ']'
}

# <1:name> <2:txt> <3:color>
function block()
{
    local color="$3"
    
    if [ -z "$color" ]
    then
	local color="#ffffff"
    fi
    
    echo -n ",{\"name\":\"$1\","
    echo -n "\"full_text\":\"$2\""
    echo -n ", \"color\":\"$color\"}"
}

function block_time()
{
    block "date" "$(date)"
}

function block_battery()
{
    local dir="/sys/class/power_supply/BAT0"
    local percent="$(cat $dir/capacity)"
    local status="$(cat $dir/status)"
    
    local color=""
    local output="$percent%"

    if [ "$status" == "Charging" ]
    then
	local color="$COLOR_INFO"
	local output="[$percent%]"
    else
	if [ "$percent" -gt 30 ]
	then
	    local color="$COLOR_SUCCESS"
	else
	    local color="$COLOR_FAILURE"
	fi
    fi
    
    block "battery" "B : $output" "$color"
}

function block_wifi()
{
    local ip="$(ip addr | grep wlan | grep inet | tr -s ' ' | cut -d' ' -f3)"
    local status="$(ip link | grep wlan | cut -d' ' -f9)"
    local color="$COLOR_SUCCESS"
    local output="$ip"
    
    if [ "$status" == "UP" ]
    then
	local output="W : $(nmcli d w | grep \* | grep -v SSID | tr -s ' ' | cut -d' ' -f2) [$ip]"
	block "wifi" "$output" "$color"
    fi
}

function block_volume()
{
    local percent="$(amixer get Master | grep % | head -n1 | tr -s ' ' | cut -d'[' -f2 | tr -dc '0-9')"
    local status="$(amixer get Master | grep % | head -n1 | tr -s ' ' | cut -d'[' -f3 | tr -dc 'a-z')"
    local color="$COLOR_SUCCESS"

    if [ "$status" == "off" ]
    then
	local color="$COLOR_FAILURE"
    fi
    
    block "volume" "V : $percent%" "$color"
}

echo -n '{"version":1}'

echo -n '['
echo -n '[]'

while true
do
    begin
    block_volume
    block_wifi
    block_battery
    block_time
    end
    
    sleep 0.2
done
